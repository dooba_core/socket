/* Dooba SDK
 * Generic Socket Library
 */

#ifndef	__SOCKET_IFACE_H
#define	__SOCKET_IFACE_H

// External Includes
#include <stdint.h>
#include <stdarg.h>

// Internal Includes
#include "socket.h"

// Name Max Length
#define	SOCKET_IFACE_NAME_MAXLEN			16

// Socket Interface Method Types
typedef	uint8_t (*socket_iface_open_udp_t)(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port);
typedef	uint8_t (*socket_iface_cnct_t)(void *ifdata, struct socket **s, char *host, uint8_t host_l, uint16_t port);
typedef	uint8_t (*socket_iface_lstn_t)(void *ifdata, struct socket **s, uint16_t port);
typedef	uint8_t (*socket_iface_send_t)(void *ifdata, struct socket *s, uint8_t *data, uint16_t size);
typedef	uint8_t (*socket_iface_get_peer_t)(void *ifdata, struct socket *s, char *ip, uint16_t *port);
typedef	void (*socket_iface_close_t)(void *ifdata, struct socket *s);

// Socket Interface Structure
struct socket_iface
{
	// Name
	char name[SOCKET_IFACE_NAME_MAXLEN];
	uint8_t name_len;

	// Network Interface Data
	void *ifdata;

	// Open UDP
	socket_iface_open_udp_t open_udp;

	// Connect Socket (Client)
	socket_iface_cnct_t cnct;

	// Bind Socket (Server)
	socket_iface_lstn_t lstn;

	// Send Data
	socket_iface_send_t send;

	// Get Peer Info
	socket_iface_get_peer_t get_peer;

	// Close Socket
	socket_iface_close_t close;

	// Next
	struct socket_iface *next;
};

// Initialize Interface
extern void socket_iface_init(struct socket_iface *iface, void *ifdata, socket_iface_open_udp_t open_udp, socket_iface_cnct_t cnct, socket_iface_lstn_t lstn, socket_iface_send_t send, socket_iface_get_peer_t get_peer, socket_iface_close_t close, char *name_fmt, ...);

#endif
