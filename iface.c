/* Dooba SDK
 * Generic Socket Library
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>

// Internal Includes
#include "iface.h"

// Initialize Interface
void socket_iface_init(struct socket_iface *iface, void *ifdata, socket_iface_open_udp_t open_udp, socket_iface_cnct_t cnct, socket_iface_lstn_t lstn, socket_iface_send_t send, socket_iface_get_peer_t get_peer, socket_iface_close_t close, char *name_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, name_fmt);

	// Setup Socket Interface
	iface->name_len = cvsnprintf(iface->name, SOCKET_IFACE_NAME_MAXLEN, name_fmt, ap);
	iface->ifdata = ifdata;
	iface->open_udp = open_udp;
	iface->cnct = cnct;
	iface->lstn = lstn;
	iface->send = send;
	iface->get_peer = get_peer;
	iface->close = close;
	iface->next = 0;

	// Release Args
	va_end(ap);
}
