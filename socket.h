/* Dooba SDK
 * Generic Socket Library
 */

#ifndef	__SOCKET_H
#define	__SOCKET_H

// External Includes
#include <stdint.h>
#include <url/url.h>

// Partial Declarations
struct socket;
struct socket_iface;

// Socket User Callback Types
typedef	void (*socket_cnct_handler_t)(void *user, struct socket *s);
typedef	void (*socket_recv_handler_t)(void *user, struct socket *s, uint8_t *data, uint16_t size);
typedef	void (*socket_dcnt_handler_t)(void *user, struct socket *s);

// Socket Structure
struct socket
{
	// Network Interface
	struct socket_iface *iface;

	// Interface Socket ID
	uint32_t id;

	// Callback User Data
	void *user;

	// Server User Data
	void *srv_data;

	// Callbacks
	socket_cnct_handler_t cnct_handler;
	socket_recv_handler_t recv_handler;
	socket_dcnt_handler_t dcnt_handler;
};

// Socket Interfaces
extern struct socket_iface *socket_ifaces;

// Initialize Sockets
extern void socket_init();

// Open UDP (Create UDP Socket through Interface)
extern uint8_t socket_open_udp(struct socket_iface *iface, struct socket **s, char *host, uint16_t port, void *user, socket_recv_handler_t recv_handler);

// Open UDP (Create UDP Socket through Interface) - Fixed Length Host
extern uint8_t socket_open_udp_n(struct socket_iface *iface, struct socket **s, char *host, uint8_t host_l, uint16_t port, void *user, socket_recv_handler_t recv_handler);

// Connect (Create Client Socket through Interface)
extern uint8_t socket_cnct(struct socket_iface *iface, struct socket **s, char *host, uint16_t port, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler);

// Connect (Create Client Socket through Interface) - Fixed Length Host
extern uint8_t socket_cnct_n(struct socket_iface *iface, struct socket **s, char *host, uint8_t host_l, uint16_t port, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler);

// Connect (Create Client Socket through Interface) using URL
extern uint8_t socket_cnct_u(struct socket_iface *iface, struct socket **s, struct url *u, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler);

// Listen (Create Server Socket through Interface)
extern uint8_t socket_lstn(struct socket_iface *iface, struct socket **s, uint16_t port, void *user, socket_cnct_handler_t cnct_handler, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler);

// Send Data through Socket
extern uint8_t socket_send(struct socket *s, void *data, uint16_t size);

// Get Socket Peer Info (address & port of remote host)
extern uint8_t socket_get_peer(struct socket *s, char *ip, uint16_t *port);

// Close Socket
extern void socket_close(struct socket *s);

// Setup new Client Socket for Server
extern void socket_srv_client_setup(struct socket *srv, struct socket *s);

// Tear down Socket
extern void socket_teardown(struct socket *s);

// Register Interface
extern void socket_register_iface(struct socket_iface *iface);

#endif
