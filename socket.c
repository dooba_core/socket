/* Dooba SDK
 * Generic Network Stack
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>

// Internal Includes
#include "iface.h"
#include "socket.h"

// Socket Interfaces
struct socket_iface *socket_ifaces;

// Initialize Sockets
void socket_init()
{
	// Clear Interfaces
	socket_ifaces = 0;
}

// Open UDP (Create UDP Socket through Interface)
uint8_t socket_open_udp(struct socket_iface *iface, struct socket **s, char *host, uint16_t port, void *user, socket_recv_handler_t recv_handler)
{
	// Open UDP
	return socket_open_udp_n(iface, s, host, strlen(host), port, user, recv_handler);
}

// Open UDP (Create UDP Socket through Interface) - Fixed Length Host
uint8_t socket_open_udp_n(struct socket_iface *iface, struct socket **s, char *host, uint8_t host_l, uint16_t port, void *user, socket_recv_handler_t recv_handler)
{
	struct socket *sp;

	// Open UDP
	if(iface->open_udp(iface->ifdata, s, host, host_l, port))				{ *s = 0; return 1; }

	// Setup Socket
	sp = *s;
	sp->iface = iface;
	sp->user = user;
	sp->recv_handler = recv_handler;

	return 0;
}

// Connect (Create Client Socket through Interface)
uint8_t socket_cnct(struct socket_iface *iface, struct socket **s, char *host, uint16_t port, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler)
{
	// Connect
	return socket_cnct_n(iface, s, host, strlen(host), port, user, recv_handler, dcnt_handler);
}

// Connect (Create Client Socket through Interface) - Fixed Length
uint8_t socket_cnct_n(struct socket_iface *iface, struct socket **s, char *host, uint8_t host_l, uint16_t port, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler)
{
	struct socket *sp;

	// Connect
	if(iface->cnct(iface->ifdata, s, host, host_l, port))					{ *s = 0; return 1; }

	// Setup Socket
	sp = *s;
	sp->iface = iface;
	sp->user = user;
	sp->recv_handler = recv_handler;
	sp->dcnt_handler = dcnt_handler;

	return 0;
}

// Connect (Create Client Socket through Interface) using URL
uint8_t socket_cnct_u(struct socket_iface *iface, struct socket **s, struct url *u, void *user, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler)
{
	// Connect
	return socket_cnct_n(iface, s, url_host(u), u->host_l, u->port, user, recv_handler, dcnt_handler);
}

// Listen (Create Server Socket through Interface)
uint8_t socket_lstn(struct socket_iface *iface, struct socket **s, uint16_t port, void *user, socket_cnct_handler_t cnct_handler, socket_recv_handler_t recv_handler, socket_dcnt_handler_t dcnt_handler)
{
	struct socket *sp;

	// Listen
	if(iface->lstn(iface->ifdata, s, port))									{ *s = 0; return 1; }

	// Setup Socket
	sp = *s;
	sp->iface = iface;
	sp->user = user;
	sp->cnct_handler = cnct_handler;
	sp->recv_handler = recv_handler;
	sp->dcnt_handler = dcnt_handler;

	return 0;
}

// Send Data through Socket
uint8_t socket_send(struct socket *s, void *data, uint16_t size)
{
	// Send
	if(s->iface == 0)														{ return 1; }
	return s->iface->send(s->iface->ifdata, s, (uint8_t *)data, size);
}

// Get Socket Peer Info (address & port of remote host)
uint8_t socket_get_peer(struct socket *s, char *ip, uint16_t *port)
{
	// Get Peer Info
	if(s->iface == 0)														{ return 1; }
	return s->iface->get_peer(s->iface->ifdata, s, ip, port);
}

// Close Socket
void socket_close(struct socket *s)
{
	// Tear down socket
	s->user = 0;
	s->cnct_handler = 0;
	s->recv_handler = 0;
	s->dcnt_handler = 0;

	// Close
	if(s->iface)															{ s->iface->close(s->iface->ifdata, s); }

	// Finish socket
	s->iface = 0;
}

// Setup new Client Socket for Server
void socket_srv_client_setup(struct socket *srv, struct socket *s)
{
	// Setup
	s->iface = srv->iface;
	s->user = srv->user;
	s->recv_handler = srv->recv_handler;
	s->dcnt_handler = srv->dcnt_handler;

	// Inform Connect Handler
	if(srv->cnct_handler)													{ srv->cnct_handler(srv->user, s); }
}

// Tear down Socket
void socket_teardown(struct socket *s)
{
	// Inform Disconnect Handler
	if(s->dcnt_handler)														{ s->dcnt_handler(s->user, s); }

	// Tear down socket
	s->iface = 0;
	s->user = 0;
	s->cnct_handler = 0;
	s->recv_handler = 0;
	s->dcnt_handler = 0;
}

// Register Interface
void socket_register_iface(struct socket_iface *iface)
{
	// Register
	iface->next = socket_ifaces;
	socket_ifaces = iface;
}
